#+property: header-args :results silent
* Requirements in Emacs
Install sly. Using use-package:
#+BEGIN_SRC elisp
  (use-package sly
    :commands (sly sly-connect))
#+END_SRC

Or with package install:

#+BEGIN_SRC elisp
(package-install 'sly)
#+END_SRC

* Requirements in the system
Install nyxt...

#+BEGIN_SRC fish
sudo pacman -S nyxt
#+END_SRC

Close all nyxt instances.

#+BEGIN_SRC fish
pkill nyxt
#+END_SRC

Install SBCL

#+BEGIN_SRC fish
sudo pacman -S sbcl
#+END_SRC

Install [[https://www.quicklisp.org/beta/#installation][quicklisp]] and check the signature validity against the public key in GnuPG servers.

#+BEGIN_SRC fish
curl -O https://beta.quicklisp.org/quicklisp.lisp
curl -O https://beta.quicklisp.org/quicklisp.lisp.asc
gpg2 --receive-keys 307965AB028B5FF7
gpg2 --verify quicklisp.lisp.asc quicklisp.lisp
#+END_SRC

Load the lisp file with sbcl:

#+BEGIN_SRC fish
sbcl --load quicklisp.lisp
#+END_SRC

Inside the sbcl terminal run to start the installation procedure (copy paste the following code):

#+BEGIN_SRC lisp
(quicklisp-quickstart:install)
#+END_SRC

It will create the file://~/quicklisp/ directory with all the package. The ~ql:quickload~ function will download, install, and load the desired package.

* Configure Nyxt
Write this at the end of ~/.config/nyxt/init.lisp

#+BEGIN_SRC lisp 
(load "~/quicklisp/setup.lisp")
(ql:quickload :slynk)
(define-command-global start-slynk (&optional (slynk-port *swank-port*))
  "Start a Slynk server that can be connected to, for instance, in
Emacs via SLY.

Warning: This allows Nyxt to be controlled remotely, that is, to execute
arbitrary code with the privileges of the user running Nyxt.  Make sure
you understand the security risks associated with this before running
this command."
  (slynk:create-server :port slynk-port :dont-close t :interface "0.0.0.0")
  (echo "Slynk server started at port ~a" slynk-port))
#+END_SRC

Consider changing ~:interface 0.0.0.0~ to ~:interface 127.0.0.1~ to avoid opening ports on all interfaces.

* Connect Emacs!
Run nyxt.

#+BEGIN_SRC fish
nyxt
#+END_SRC

Press ~M-x start-slynk~ to start the server.

Run ~M-x sly-connect~, provide the host and port at the Emacs side (or run the following with C-c C-c):

#+BEGIN_SRC elisp
  (sly-connect "0.0.0.0" "4006")
#+END_SRC

* Controlling Nyxt
Functions can be redefined with ~defun~.

Nyxt commands are under the package ~nyxt::~. For instance, the following command will open the manual.

#+BEGIN_SRC lisp
(nyxt::manual)
#+END_SRC

Usually, nyxt::* is not required. The following command will open a new URL in the current buffer:

#+BEGIN_SRC lisp
(buffer-load "about:blank")
#+END_SRC

** Getting help
#+BEGIN_SRC lisp
  (describe #'buffer-load)
#+END_SRC

** Nuevas funciones con HTML

#+BEGIN_SRC lisp
  (define-command my-html-data ()
    "Just open an example buffer"
      (with-current-html-buffer (buffer "*My help*" 'nyxt/help-mode:help-mode)
        (spinneret:with-html-string
          (:style (style buffer))
          (:style (cl-css:css '(("#documentation .button"
                                 :min-width "100px"))))
          (:h1 "Welcome to Nyxt :-)")
          (:p (:a :href "https://nyxt.atlas.engineer" "https://nyxt.atlas.engineer"))
          (:p (:a :class "button" :href (lisp-url `(help)) "Help"))
          (:p "This is a common example text."))))
#+END_SRC

This function is accessible via de URL lisp://(my-html-data)

** Setting a slot
A slot works as class variables shared among instances (classes and instances are OOP simile). It can be setted with ~#'define-configuration~ functions. As the name suggests, they are used as configurations variables because all instances are affected as soon as their values are modified.

The following example set a slot ~prompter::filter-postprocessor~ with a lambda function as value (the function is the value). The slot is defined in the ~user-new-url-or-search-source~ class (the name is not case sensitive).

#+BEGIN_SRC lisp
  (define-configuration USER-NEW-URL-OR-SEARCH-SOURCE
    ((prompter::filter-postprocessor
      (lambda (nyxt::suggestions nyxt::source nyxt::input)
        (declare (ignore nyxt::suggestions nyxt::source))
        (nyxt::input->queries nyxt::input :check-dns-p t :engine-completion-p nil)))))
#+END_SRC


** Classes
Classes are the same as OOP, but instances can change its behaviour by changing the method and slot definitions.

That is the reason why slots are used as configuration too.

The following create an instance of a class ~user-new-url-or-search-source~.

#+BEGIN_SRC lisp
  (make-instance 'user-new-url-or-search-source :actions an-action-list-here)
#+END_SRC


* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  How to connect Emacs with Nyxt
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   01 dic 2021
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
