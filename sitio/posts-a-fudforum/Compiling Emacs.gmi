# Compiling Emacs 28

# Manjaro: archlinuxcn repository and install required packages

Install [archlinuxcn] repositories by adding the following into /etc/pacman.conf: 

```Configure section to add archlinuxcn repository.
[archlinuxcn]
Server = http://repo.archlinuxcn.org/$arch
## or install archlinuxcn-mirrorlist-git and use the mirrorlist
#Include = /etc/pacman.d/archlinuxcn-mirrorlist
```

Then, update local repositorie, install the new keyring, and libgccjit. Compiling libgccjit takes a long time! 

```Installing required packages in Manjaro.
sudo pacman -Sy
sudo pacman -S archlinuxcn-keyring
sudo pacman -S libgccjit
sudo pacman -S mailutils
```


=> https://wiki.archlinux.org/title/unofficial_user_repositories#archlinuxcn archlinuxcn


# Clone Emacs

Clone the Emacs repository. A shallow clone can be made with `--depth 1 ` parameter, unless the user want to collaborate with the development. 

```Cloning and downloading Emacs source code.
git clone --depth 1 https://git.savannah.gnu.org/git/emacs.git
cd emacs
```

Depending on the user's choice, two versions are available: Emacs 28 and 29. The user can install one of these versions.  


## Emacs 28

To install Emacs 28, change the branch to `emacs-28 ` first. 

```Switching to the Emacs 28 branch with git.
git fetch --depth 1 origin emacs-28
git checkout FETCH_HEADER
git switch -c emacs-28
```


## Emacs 29

To install Emacs 29, change the branch to `emacs-29 ` first. 

```Switching to the Emacs 29 branch with git.
git fetch --depth 1 origin emacs-29
git checkout FETCH_HEADER
git switch -c emacs-29
```


# Compiling

Start compiling Emacs! Native compilation are enabled. 

For X11 users: 

```Commands to compile for the X11 windows server.
./autogen.sh
./configure --with-native-compilation --with-mailutils --with-imagemagick --with-xft --with-xaw3d --with-xwidgets
make -j2
```

For Wayland users the PGTK package should be added (X11 will work too!): 

```Commands to compile for the Wayland windows server.
./autogen.sh
./configure --with-native-compilation --with-mailutils --with-imagemagick --with-xft --with-xaw3d  --with-pgtk --with-json
make -j2
```

The `j2 ` parameter means run 2 jobs in parallel.  

It should report the following or similar output: 

```Output of the configure program.
Configured for 'x86_64-pc-linux-gnu'.

  Where should the build process find the source code?    .
  What compiler should emacs be built with?               gcc -g3 -O2
  Should Emacs use the GNU version of malloc?             no
    (The GNU allocators don't work with this system configuration.)
  Should Emacs use a relocating allocator for buffers?    no
  Should Emacs use mmap(2) for buffer allocation?         no
  What window system should Emacs use?                    x11
  What toolkit should Emacs use?                          GTK3
  Where do we find X Windows header files?                Standard dirs
  Where do we find X Windows libraries?                   Standard dirs
  Does Emacs use -lXaw3d?                                 no
  Does Emacs use -lXpm?                                   yes
  Does Emacs use -ljpeg?                                  yes
  Does Emacs use -ltiff?                                  yes
  Does Emacs use a gif library?                           yes -lgif
  Does Emacs use a png library?                           yes -lpng16 -lz 
  Does Emacs use -lrsvg-2?                                yes
  Does Emacs use cairo?                                   yes
  Does Emacs use -llcms2?                                 yes
  Does Emacs use imagemagick?                             yes
  Does Emacs use native APIs for images?                  no
  Does Emacs support sound?                               yes
  Does Emacs use -lgpm?                                   yes
  Does Emacs use -ldbus?                                  yes
  Does Emacs use -lgconf?                                 no
  Does Emacs use GSettings?                               yes
  Does Emacs use a file notification library?             yes -lglibc (inotify)
  Does Emacs use access control lists?                    yes -lacl
  Does Emacs use -lselinux?                               no
  Does Emacs use -lgnutls?                                yes
  Does Emacs use -lxml2?                                  yes
  Does Emacs use -lfreetype?                              yes
  Does Emacs use HarfBuzz?                                yes
  Does Emacs use -lm17n-flt?                              yes
  Does Emacs use -lotf?                                   yes
  Does Emacs use -lxft?                                   no
  Does Emacs use -lsystemd?                               yes
  Does Emacs use -ljansson?                               yes
  Does Emacs use the GMP library?                         yes
  Does Emacs directly use zlib?                           yes
  Does Emacs have dynamic modules support?                yes
  Does Emacs use toolkit scroll bars?                     yes
  Does Emacs support Xwidgets?                            yes
  Does Emacs have threading support in lisp?              yes
  Does Emacs support the portable dumper?                 yes
  Does Emacs support legacy unexec dumping?               no
  Which dumping strategy does Emacs use?                  pdumper
  Does Emacs have native lisp compiler?                   yes
```


# Updating Emacs

Once installed, Emacs can be updated by using the commands shown in this section. Remember that the development team updates the code frequently. 

Before updating, the following line takes note of the git version used. This is useful in case it is required to rollback the compilation. 

```Git command to save the curren commit.
git show > ../emacs-29-my-last-commit.txt
```

Now, the following commands updates and start the compilation.  

```Getting latest source code and compiling.
git pull
./autogen.sh
./configure --with-native-compilation --with-mailutils --with-imagemagick --with-xft --with-xaw3d  --with-pgtk --with-json
make -j2
```

It is possible that some errors may appear. In that case, consider cleaning the compiled files before trying the new code: 

```Getting latest source code, cleaning, and compiling.
git pull
./autogen.sh
./configure --with-native-compilation --with-mailutils --with-imagemagick --with-xft --with-xaw3d  --with-pgtk --with-json
make clean
make -j2
```

If errors still appear, checkout the commit version noted before and, if you cleaned the compiled code, recompile your previous version. Sometimes, the deveolpment team may commit a new version very soon, so check frequently for bugfixes. 
