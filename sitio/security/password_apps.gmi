# Password apps

# Password focused

Tags: 
* store :: 

* generator :: 

* hasher :: 


## F-Droid apps

### Password Store

Manage your passwordsGPL-3.0-only 


#### AuthPass - KeePass compatible Password Manager

Keep your passwords safe across all platforms and devices.GPL-3.0-only 


#### aRevelation

password manager based on Revelation Password Manager file formatGPL-3.0-only 


#### Peanut Encryption

Encrypted password manager. Only local storedApache-2.0 


#### Passky

simple, modern and open source password managerGPL-3.0-only 


#### PasswdSafe

Port of PasswordSafe password managerArtistic-2.0 


#### aRevelation

Password managerGPL-3.0-only 


#### NC Passwords

A password manager app for Nextcloud's Passwords appApache-2.0 


#### OI Safe

Password and private data managerApache-2.0 


#### rust-keylock-android

A password manager with goals to be Secure, Simple to use, Portable and ExtensibGPL-3.0-only 


#### KeePassDroid

KeePass-compatible password safeGPL-2.0-or-later 


#### Authorizer

Password Manager with USB Keyboard emulationGPL-3.0-only 


#### NextcloudPasswords

Manage your Nextcloud passwordsGPL-3.0-only 


#### KeePassDX

Secure and open source password manager compatible with KeePass files.GPL-3.0-or-later 


##### Portability

Compatible with KeePassXC for Desktop. Firefox and other browser provide plugins that connect to the application. 

See [KeePassXC homepage]. 


=> https://keepassxc.org/ KeePassXC homepage


#### Passman

Allow you to manage the credentials stored in your Nextcloud Passman app.GPL-3.0-only 


### Generators

#### Frog Password Generator

Helps to remember passwordsGPL-3.0-only 


#### Wassword - Wonderful Password Generator

Create complex password at your fingertips!MIT 


#### Diceware Password Generator

Generate diceware passwordsGPL-3.0-only 


#### Password Generator

Password Generator is an app for generating secure passwordsApache-2.0 


#### Password Generator (Privacy Friendly)

Generate passwordsGPL-3.0-only 


#### Passera

Generate strong passwordsGPL-3.0-only 


### Hashers

These are password hasher programs. They are programs that generate the same password for the same tag and main password. 

With a password hasher, you only need to keep note the program or Website tag, the bump version (if you change several times the password). The main password can be anything you remember, and it must not be writen down anywhere.

* Some Theory:
=> https://crypto.stanford.edu/PwdHash/

* Summary:

- 1) Password Hash /PwdHash
- 2) LessPass
- 3) Sesam
- 4) HashPass
- 5) PassCard
- 6) PasswordMaker Pro
- 7) Salasana
- 8) Hash it!
- 9) Twik
- 10) CryptoPass
- 11) SuperGenPass
#### Password Hash / PwdHash

[https://f-droid.org/es/packages/com.uploadedlobster.PwdHash/] Create passwords for each websiteBSD-3-Clause 


=> https://f-droid.org/es/packages/com.uploadedlobster.PwdHash/ https://f-droid.org/es/packages/com.uploadedlobster.PwdHash/

* Advantages

- One main implementation in its own Webpage. 
- Very simple: it just ask for site-tag and master-password. 


* Disadvantages

- Uses MD5, main version use HMAC_MD5. 
- Cannot choose password properties: only numbers, special characters, length. 


#### LessPass

Generate unique passwords for your accounts based on a master password 

License: GPL-3.0-only 


#### Sesam

* F-droid:
=> https://f-droid.org/es/packages/de.larcado.sesam/
* Gitlab:
=> https://gitlab.com/larcado/sesam/tree/HEAD
* Features/Issues
- Hash based password manager.
- License: Apache-2.0.
- Only for Android 8 or higher.
- Last release: 1.1.0 (2018).
- Based on a salted hash function PBKDF2WithHmacSHA1.
- We must define a domain before, if we try to generate a password without a previously defined domain the application suddenly closes!
- The user can predefine the salt, password length, password scheme, iterations and hash bits.
- Support for Oreo's (Android 8) Autofill API.

* ¿How does it work?
"It's quite simple. You remember a master password. It is important that nobody knows it, so keep it secure :D
Second, there is a domain to which you want to generate a password.

Step 1
Simple string concatenation
base = domain + master password

Step 2
The Hash-Algorithm PBKDF2WithHmacSHA1 will be used alongside a user pre defined salt.
By default, a 256-bit hash is created from the base with 32,000 iterations.

Step 3
The generated 256-bit hash will be interpreted as a BigInteger.
Now there are (pre defined by user settings):

allowedCharacter = lowerCaseLetters + upperCaseLetters + numbers + specialCharacters

The BigInteger is calculated modulo the number of allowed characters. This remainder corresponds to the xth character of the allowed Characters (First character of the password).
Now the BigIntger is divided by the desired length of the resulting password.
This process is repeated until all characters have been determined."

* Screenshots

=> file:imgs/sesam-screenshot.png file:imgs/sesam-screenshot.png


#### HashPass

[https://f-droid.org/es/packages/byrne.utilities.hashpass/] Use hashes as passwordsGPL-3.0-only 


=> https://f-droid.org/es/packages/byrne.utilities.hashpass/ https://f-droid.org/es/packages/byrne.utilities.hashpass/


#### PassCard

Generate password cardGPL-3.0-only 


#### PasswordMaker Pro

[https://f-droid.org/es/packages/org.passwordmaker.android/] Password makerGPL-3.0-only 

PasswordMaker Pro creates unique, secure passwords that are very easy for you to retrieve but no one else. Nothing is stored anywhere, anytime, so there’s nothing to be hacked, lost, or stolen. See [https://passwordmaker.org/] for details. 


=> https://f-droid.org/es/packages/org.passwordmaker.android/ https://f-droid.org/es/packages/org.passwordmaker.android/
=> https://passwordmaker.org/ https://passwordmaker.org/


#### Salasana

Generates site-specific unique passwordsBSD-3-Clause 


#### Hash It!

[https://f-droid.org/es/packages/com.ginkel.hashit/] Use strong passwordsGPL-3.0-only 

Compatible with  


=> https://f-droid.org/es/packages/com.ginkel.hashit/ 


#### Twik

Manage and generate secure passwordsGPL-3.0-only 

Use a combination of a master key/password, a private key, and the website name/url to generate a unique, strong password. Since passwords are generated each time, even if one of them is compromised the rest would be safe. Twik also integrates with any web browser, so that you can generate a password quickly by sharing a website with Twik from the browser. 

It is compatible with Password Hasher Plus, a Chrome extension by Eric Woodruff that follows the same principles for generating strong passwords. You can use the same private and master keys to generate the same passwords on your desktop browser. 

* F-droid:
=> https://f-droid.org/es/packages/com.reddyetwo.hashmypass.app/ 

* Github:
=> https://github.com/gustavomondron/twik

* Features: 

- Several profiles, each with its own private key 
- Favicons to easily identify websites 
- Identicons to check that you typed your master key correctly at a glance 
- Share any website from a web browser to generate a password for it 
- Customize password generation for each website (password length and characters) 
- Automatically copy generated passwords to the clipboard 


#### CryptoPass

Strong passwords for different websites using a single secret keyMIT 

* Features:

- Generate different passwords from single master password using cryptographically strong method (PBKDF2 with SHA-256).

- Create strong passwords for different websites (or anything else) using a single secret key. There's no need to remember or store multiple passwords if you can derive them from your master key.

- Сan easily be used as password generator and password manager.

- Your passwords doesn't store anywhere, username and url pairs stored for easy access.

- password = base64(pbkdf2(secret, username@url))

- PBKDF2 uses SHA-256 and 5000 iterations. Cuts password to specified length (25 by default).

- Compatible with Chrome CryptoPass extension: https://chrome.google.com/webstore/detail/cryptopass/hegbhhpocfhlnjmemkibgibljklhlfco

- Last Release: 1.2 (2017)

- Only for Android 2.3 or higher

- MIT Licence

* F-droid:
=> https://f-droid.org/es/packages/krasilnikov.alexey.cryptopass/

* Bitbucket:
=> https://bitbucket.org/zeac/cryptopass/src/master/


#### SuperGenPass

* Implementation of SuperGenPassGPL-3.0-or-later 
* About:
"SuperGenPass is a different kind of password manager: instead of randomly-generating and storing passwords, it combines a Web site's domain and your master password to generate a unique password for each Web site you visit using a strong cryptographic hash.

In addition to using the form when you run the app, you can also use the "Share Page" menu item in the Browser application to launch SGP and pre-fill the domain. Go to Menu → More → Share Page.

This app also includes a strong PIN generation algorithm. It's a slightly modified version of RFC4226 HOTP (modified to work with domains instead of numerical counters) and is designed to never generate easily-guessable pins, such as 1234, 0000, 1985, 8675309, or 0007. For more details on how it works, please see the source."

* Web Sites:
=> https://staticfree.info/projects/sgp/
=> https://chriszarate.github.io/supergenpass/

* F-droid:
=> https://f-droid.org/es/packages/info.staticfree.SuperGenPass/

* Github:
=> https://github.com/xxv/SuperGenPass

* Ruby Gem:
=> https://github.com/rstacruz/sgpass

* Features:
- Copy gen'd password: Copy the generated password when enter is pressed on the keyboard.
- Autocomplete domains: Remember any domains you've used.
- Generates a strong PIN as well as a password (PIN lengh: 3 to 8).
- Show visual hash: show a graphical SHA1 has of the master password to aid entry.
- The user can predefine the salt, password length (4 to 24).
- Password Generator Type: MD5, Sha-512, Password Composer.
- Generate and set salt: Generates a random 4096-bit base64-encoded salt. Once generated, it sets the salt and displays it as a QR code.
- Scan Salt: Load a salt by scanning a QR Code.
- Verify master password.
- Last Release Versión 3.0.0 (2015).
- Only for Android 4.0.3 or higher.
- Licence GPL v3.



### OTP

#### mOTP

One-time password generatorApache-2.0 


### Utilities

#### IYPS

A password strength test app with crack times, warnings and suggestions.GPL-3.0-only 


#### My Wifi Passwords

View your Wi-Fi PasswordsGPL-3.0-only 


#### WiFiKeyShare

Share Wi-Fi passwords with QR codes and NFC tagsGPL-3.0-or-later 


### Unknown

#### Password Master

Password Master generates and manages passwords in a secure encrypted databaseApache-2.0 


#### Pushie

Provides temporary, secure, password sharingApache-2.0 


#### Puff

Password UtilityMIT 


#### Duress

Duress password triggerGPL-3.0-only 


#### EncPassChanger

Use a stronger disk passwordBSD-2-Clause 


#### SnooperStopper

Set different boot and unlock passwordsGPL-3.0-only 


#### NoteBuddy

Store encrypted notesGPL-3.0-only 


#### NextcloudServices

Simply fetch Nextcloud notifications on devices without Google Play servicesGPL-3.0-only 


#### Note Crypt Pro

Keep your notes safe and secureGPL-3.0-only 


#### Cmus Remote

Remotely control CMUS instancesGPL-2.0-only 


#### SealNote

Simple, safe and easy to use notepadMIT 


#### ANOTHERpass

Another way to manage passwordsMIT 


#### Sentry

Enforce security policiesGPL-3.0-only 


#### Cooper

Remote file editingGPL-3.0-only 


#### TinyKeePass

Another simple read-only KeePass appApache-2.0 


#### NFC Key

Unlock KeePass database with NFCGPL-3.0-or-later 


 #### BlitzMail

Share content and notes via emailAGPL-3.0-or-later 


#### AppLocker

Lockdown your appsGPL-3.0-only 


#### KeyPass

Offline, Secure, PrivateMIT 


#### Pi Locker

LockscreenGPL-2.0-only 


#### Clipboard Cleaner

Check and clean your clipboardMIT 


#### YubiClip

Copy YubiKey NEO OTP from NFC to clipboardBSD-2-Clause 


#### miniNoteViewer

Note taker with encryptionGPL-2.0-or-later 


#### OpenPass

Reimplementation of SafeNet MobilePassBSD-2-Clause 


#### Deck Wallet

Store your Bitcoins in a deck of cardsMIT 


#### DO Swimmer

DigitalOcean droplet managerMIT 


#### Aegis Authenticator

Free, secure and open source 2FA app to manage tokens for your online servicesGPL-3.0-only 


#### Stingle Photos

Stingle Photos is a secure, end-to-end encrypted gallery and sync appGPL-3.0-only 


#### Sav PDF Viewer Pro - Read PDF files safely

The simplest PDF viewerGPL-3.0-only 


#### TUC WLAN

Secure configuration of eduroam at TU ChemnitzApache-2.0 


#### Tryton

Enterprise resource managementGPL-3.0-only 


#### PDF Creator

Create and edit PDF filesGPL-3.0-or-later 


#### Dinomail

App to interact with DinoMail.GPL-3.0-or-later 


#### FON Access

Auto connect to FON networkGPL-3.0-only 


#### Auth Token

OATH software tokensGPL-3.0-only 


#### NetTTS

Text-to-Speech over networkGPL-3.0-only 


#### SelfPrivacy

Self-hosted services without painGPL-2.0-or-later 


#### uNote

Lightweight and minimalist notepadGPL-3.0-only 


 #### Kotatsu

Manga reader with online cataloguesGPL-3.0-or-later 


#### Anemo

A private local storage utility application for android.GPL-3.0-only 


#### Router Keygen YoloSec

Generate default WPA/WEP keysGPL-3.0-only 


#### Twidere

Microblogging clientGPL-3.0-or-later 


#### Secure File Manager Beta

File manager for keeping your files in safeGPL-3.0-or-later 


#### Binary Kitchen Doorlock

Binary Kitchen's Open SesameGPL-2.0-only 


#### Overchan

Browse multiple imageboardsGPL-3.0-only 


#### FreeOTP

Two-factor authenticationApache-2.0 


#### AndrOBD MQTTPublisher

Plugin extension for AndrOBD to publish OBD measurements to MQTT home automationGPL-3.0-or-later 


#### Device Connect

Yet another tool to connect Android phone with desktopMIT 


#### drip. menstrual cycle and fertility tracking

Menstrual cycle tracking helps you understand your cycle data and gives you insiGPL-3.0-only 


#### RasPi Check

Check the status of your RasPiMIT 


#### CrocodileNote

Take notes and encrypt themGPL-3.0-only 


#### Fon

Auto connect to FON networkGPL-3.0-only 


#### Morse

Convert text into Morse code and vice-versaApache-2.0 


#### Overchan (fork)

Browse multiple imageboardsGPL-3.0-only 


#### dawdle

Moodle clientGPL-3.0-or-later 


#### Ghost Commander - SMB plugin

Access files on the networkGPL-3.0-only 


#### Compass Keyboard

International keyboardBSD-3-Clause 


#### treehouses remote

Communicate with headless Raspberry Pi mobile server running treehouses image viAGPL-3.0-only 


#### Dib2Calc

The crazy calculator (RPN mode). Once you get used to it, you will love it :-)GPL-3.0-or-later 


#### EasySSHFS

Sshfs with ssh client and interface for Android.MIT 


#### Mebis (Unofficial)

Displays mobile Mebis website.GPL-3.0-or-later 


#### Ghost Commander - Samba plugin

Access files on the networkGPL-3.0-only 


#### Wifi Remote Play

Media remote controlGPL-3.0-or-later 


#### AirUnlock

Controlling Mac lock stateApache-2.0 


#### FOSS Moodle

Interact with HHS moodle instanceGPL-3.0-or-later 


#### BlueWallet Bitcoin Wallet

Thin Bitcoin Wallet Built with React Native and ElectrumMIT 


#### SimpleTextCrypt

Encrypts plain text using AES256GPL-3.0-or-later 


#### EteSync - Secure Data Sync

Secure, private and end-to-end encrypted calendar, contacts and tasks syncGPL-3.0-only 


#### kMeet

Free and secure videoconferencing solutionApache-2.0 


#### Jitsi Meet

Instant video conferences efficiently adapting to your scaleApache-2.0 


#### SQRL Login

An implementation for SQRL (Secure Quick Reliable Login)MIT 


#### Tiny Travel Tracker

Secure long term GPS tracker and logGPL-3.0-only 


#### XMouse

Remotely control X11 via SSH commands generated by your phone or tabletMIT 


#### Nighthawk Wallet

Private Money in your pocket.Apache-2.0 


#### DoliDroid

Use Dolibarr ERP & CRMGPL-3.0-only 


#### ente - encrypted photo storage

ente is an end-to-end encrypted photo storage appAGPL-3.0-only 


#### FreeOTP+

Enhanced fork of FreeOTP-Android providing a feature-rich 2FA authenticatorApache-2.0 


#### Wasted

Lock a device and wipe its data on emergencyGPL-3.0-only 


#### Squeezer

Remote control for Logitech Media Server ("Squeezeserver" etc) and players.Apache-2.0 


#### Photok

Encrypt your photos on your device and keep them safe from others.Apache-2.0 


#### Tinc App

Android binding and user interface for the tinc mesh VPN daemonGPL-3.0-only 


#### unofficial Stud.IP-app

An unofficial app for Stud.IP systems.GPL-3.0-only 


#### WebApps Sandboxed Browser

WebApps turns mobi/web app sites into secure apps! Open source.MIT 


#### Green Pass PDF Wallet

Simple PDF Wallet for your most important PDFsGPL-3.0-only 


#### Presence Publisher

Regularly publish to an MQTT topicMIT 


#### Plutus - Investment Tracker

We're here to track money going out of your checking account into different typeApache-2.0 


#### DroidFS

Store & access your files securelyAGPL-3.0-only 


#### strongSwan VPN Client

An easy to use IKEv2/IPsec-based VPN client.GPL-2.0-or-later 


#### Crypto Prices

Track all of your favorite crypto pricesGPL-3.0-only 


#### SMS Backup+

Backup SMS, MMS and call history to Gmail, Google Calendar or IMAP.Apache-2.0 


#### xBrowserSync

Browser syncing as it should be: secure, anonymous and free!GPL-3.0-only 


#### LibreOffice & OpenOffice document reader | ODF

Document reader & file editor for Libreoffice & OpenOffice | ODF: ODT, ODS +moreGPL-3.0-or-later 


#### Simple File Manager Pro

Organize your Files and Folders. Simple Mobile Content Transfer, File SharingGPL-3.0-only 


#### Tutanota

Encrypted email & calendar service - easy to use, secure by design.GPL-3.0-or-later 


#### PixelKnot: Hidden Messages

Have a secret that you want to share? Why not hide it in a picture?GPL-3.0-only 


#### Kontalk Messenger

A new way of communicating: community-driven instant messaging network.GPL-3.0-only 


#### AntennaPod

Easy-to-use, flexible and open-source podcast manager and playerGPL-3.0-only 


## Browser features or add-ons

### Firefox


# Privacy focused

## F-Droid apps

### Echo

Record audio in the backgroundGPL-3.0-only 


### App Locker

Protect apps with a password or patternApache-2.0 
