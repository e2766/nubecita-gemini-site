
* Nord colours

#+BEGIN_SRC config
# Nord theme colors

set $nord0   #2E3440
set $nord1   #3B4252
set $nord2   #434C5E
set $nord3   #4C566A
set $nord4   #D8DEE9
set $nord5   #E5E9F0
set $nord6   #ECEFF4
set $nord7   #8FBCBB
set $nord8   #88C0D0
set $nord9   #81A1C1
set $nord10  #5E81AC
set $nord11  #BF616A
set $nord12  #D08770
set $nord13  #EBCB8B
set $nord14  #A3BE8C
set $nord15  #B48EAD

 colors {
        # background #004400
        # statusline #ffffff
        # separator #666666

	# # class            #border #backgd #text
      # focused_workspace  #00ff00 #004400 #ffffff
      # active_workspace   #333333 #5f676a #ffffff
      # inactive_workspace #440000 #000000 #888888
      # urgent_workspace   #ff0000 #440000 #ffffff
      # binding_mode       #2f343a #900000 #ffffff

	# background #222828
      # statusline #616D59


	# focused_workspace  #5A8DBD #383134
	# active_workspace   #5A8DBD #383134
	# inactive_workspace #A9B8B5 #616D59
	# urgent_workspace   #E93E38 #383134

	background $nord0
	statusline $nord8

	focused_workspace  $nord3  $nord8
	active_workspace   $nord3  $nord8
	inactive_workspace $nord8  $nord3
	urgent_workspace   $nord3 $nord15 

}

#+END_SRC



* Meta     :noexport:

#+name: before-save
#+BEGIN_SRC emacs-lisp :results silent
  (require 'ox-gemini)
  (org-export-to-file 'gemini "i3.gmi")
#+END_SRC

# ----------------------------------------------------------------------
#+TITLE:  i3 Window Manager
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   25 ago 2022
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# --  Info Export
#+TEXINFO_DIR_CATEGORY: A category
#+TEXINFO_DIR_TITLE: i3 Window Manager: (i3)
#+TEXINFO_DIR_DESC: One line description.
#+TEXINFO_PRINTED_TITLE: i3 Window Manager
#+TEXINFO_FILENAME: i3.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# eval: (add-hook 'before-save-hook (lambda () (org-babel-ref-resolve "before-save")) nil t)
# End:
