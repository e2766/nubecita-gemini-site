# MY RUBY CHEAT SHEET

## Basics

* 'HELLO WORLD EXAMPLE': Inside EMCAS open a new file and type: 
```
 puts "hello there!"
```

,----
| hello there!
`----


save the file as "hello.rb" Open a terminal and ruby command and pass hello.rb as argument: 
```
cat hello.rb
```

* $ irb: to write ruby in the terminal 
* don't use ' in ruby, use " instead 
* you can replace most {} with do end and vice versa –– not true for hashes or #{} escapings 
* Best Practice: end names that produce booleans with question mark 
* tag your variables: 
* $: global variable 
* @: instance variable 
* @@: class variable 
* Writing 1_000_000 is the same than 1000000??? 


# Variables, Contants, Arrays, Hashes & Symbols
