#! /usr/bin/fish
source config/certificates.fish

if contains "$TLS_CLIENT_HASH" $my_certificates
    echo -e "20 text/gemini\r\n"
    echo "# ☑️ Certificado aceptado"
    echo "HASH Recibido:"
    echo "```"
    echo "$TLS_CLIENT_HASH"
    echo "```"
    echo "Nombre: $REMOTE_USER"
else
    echo -e "20 text/gemini\r\n"
    echo "# ❎ Su certificado no es válido"
    echo "HASH Recibido:"
    echo "```"
    echo "$TLS_CLIENT_HASH"
    echo "```"
    echo "Nombre: $REMOTE_USER"      
end
