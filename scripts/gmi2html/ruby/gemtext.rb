# Copyright 2022 Christian Gimenez
#
# gemtext.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# frozen_string_literal: true

module Gemtext
  # A Gemtext Document
  #
  # A Gemtext Document is a list of GemBlock or GemLine elements.
  class Document
    def initialize
      @lines = []
    end

    attr_accessor :lines

    def add(component)
      @lines.append component
    end

    def to_s
      @lines.map(&:to_s).join
    end
  end

  # A Gemtext block type syntax
  class GemBlock
    STARTING = ''
    ENDING = ''

    def initialize(code = '', caption = '')
      @code = code
      @caption = caption
    end

    def to_s
      <<~STRING_REP
        #{STARTING}#{@caption}
        @code
        #{ENDING}
      STRING_REP
    end

    def parse_caption(line)
      @caption = line.match "#{STARTING}[[:space:]]*(.*)$"
      @caption.strip!
    end

    def parse_text_line(line)
      @caption += "#{line.trim}\n"
    end

    # Parse the line and add the caption or text to the block.
    #
    # If the line is an ending block line, return false.
    def parse_line(line)
      return false if ending? line

      if @caption.empty?
        parse_caption line
      else
        parte_text_line line
      end
    end

    def parse_input(lines)
      @caption = lines.match STARTING
    end

    class << self
      # Is the line a begining of a block?
      def parsable?(line)
        line.start_with? STARTING
      end

      def ending?(line)
        m = line.match "^#{ENDING}[[:space:]]*$"
        !m.nil?
      end
    end
  end

  # A Gemtext line type syntax
  class GemLine
    PREFIX = ''

    def initialize(text = '')
      @text = text
    end

    def to_s
      lines = @text.split("\n").map
      headers = lines.map do |s|
        "#{PREFIX} #{s}"
      end
      headers.join
    end

    class << self
      # Is the line parsable?
      def parsable?(line)
        line.start_with? PREFIX
      end
    end
  end

  # A Gemtext Link
  class Link < GemLine
    PREFIX = '=>'

    def initialize(url, description = '')
      super "#{@url} {@description}"
      @url = url
      @description = description
    end
  end

  # A Gemtext Code Block
  class CodeBlock < GemBlock
    STARTING = '```'
    ENDING = '```'
  end

  # A Gemtext heading
  class Heading < GemLine
    PREFIX = '# '
  end

  # A Gemtext Subheading
  class Subheading < GemLine
    PREFIX = '## '
  end

  # A Gemtext Subsubheading
  class Subsubheading < GemLine
    PREFIX = '### '
  end

  # A Gemtext Itemize
  class Itemize < GemLine
    PREFIX = '* '
  end

  # A Gemtext Quote block
  #
  # Despite being a "block", in reality is a line-type gemtext. Because all
  # lines start with some prefix. It is not a text surrounded by
  # strating-ending strings.
  class QuoteBlock < GemLine
    PREFIX = '>'
  end

  # Converter Gemtext to HTML.
  class HtmlBuilder
    def initialize(document)
      @document = document
    end

    def line_to_html(line_str)
    end
  end

  # TextParser
  #
  # A parser for text files.
  class TextParser
    LINE_COMPONENTS = [QuoteBlock, Itemize,
                       Heading, Subheading, Subsubheading,
                       Link].freeze
    BLOCK_COMPONENTS = [CodeBlock].freeze

    def initialize(file)
      @file = file
      @document = Document.new
    end

    attr_reader :document, :file

    def parse_next
      line = @file.gets

      if block? line
        @document.add parse_block line
      else
        @document.add parse_line line
      end
    end

    private

    def block?(line)
      component = BLOCK_COMPONENTS.index do |comp|
        comp.parsable? line
      end
      !component.nil?
    end

    def parse_block(line)
      component = BLOCK_COMPONENTS.index do |comp|
        comp.parsable? line
      end

      block = BLOCK_COMPONENTS[component].new
      line = @file.gets while block.parse_line line
      block
    end

    def parse_line(line)
      component = LINE_COMPONENTS.index do |comp|
        comp.parsable? line
      end

      LINE_COMPONENTS[component].new line
    end
  end
end
